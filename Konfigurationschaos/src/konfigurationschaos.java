
public class konfigurationschaos {

	public static void main(String[] args) {
		String typ = "Automat AVR";
		String bezeichnung = "Q2021_FAB_A";
		//Name des Automaten wird deklariert
		String name = typ + " " + bezeichnung;
		char sprachModul = 'd';
		//Systemsprache festgelegt
		final int PRUEFNR = 4;
		//Pruefnummer deklariert
		double maximum = 100.00;
		double patrone = 46.24;
		double fuellstand = maximum - patrone;
		//Berechnung und Bestimmung der F�llmenge
		int muenzenCent = 1280;
		int muenzenEuro = 130;
		int summe = muenzenCent + muenzenEuro * 100;
		int euro = summe / 100;
		int cent = summe % 100;
		
		boolean statusCheck;
		statusCheck = (euro <= 150)
		&& (fuellstand >= 50.00) 
		&& (euro >= 50)
		&& (cent != 0)
		&& (sprachModul == 'd')
		&&  (!(PRUEFNR == 5 || PRUEFNR == 6)); 
		
		
		System.out.println("Name: " + name);
		//Name des Ticketautomaten wird ausgegeben
		System.out.println("Sprache: " + sprachModul);
		//Sprache wird ausgegeben
		System.out.println("Pruefnummer : " + PRUEFNR);
		//Pr�fnummer ausgegeben
		System.out.println("Fuellstand Patrone: " + fuellstand + " %");
		//Ausgabe des F�llstandes
		System.out.println("Summe Euro: " + euro +  " Euro");
		System.out.println("Summe Rest: " + cent +  " Cent");
		//Ausgabe des Geldes
		System.out.println("Status: " + statusCheck);
		
	}

}
