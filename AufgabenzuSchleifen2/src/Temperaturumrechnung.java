import java.util.Scanner;
public class Temperaturumrechnung {

	public static void main(String[] args) {
		int Fahrenheit;
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Bitte den Startwert in Celsius eingeben: ");
		int CelsiusA = myScanner.nextInt();
		System.out.println("Bitte den Endwert in Celsius eingeben: ");
		int CelsiusE = myScanner.nextInt();
		System.out.println("Bitte die Schrittweite in Grad Celsius eingeben:");
		int CelsiusS = myScanner.nextInt();
		
		while(CelsiusA <= CelsiusE) {
			System.out.print(CelsiusA + " Celsius ");
			Fahrenheit = (CelsiusA * 9/5)+32;
			System.out.println(Fahrenheit + " Fahrenheit ");
			 CelsiusA = CelsiusA + CelsiusS;
			
		}
	}

}
