import java.util.Scanner;

public class Quersumme {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie eine Zahl ein: ");
		int Zahl = myScanner.nextInt();
		

		int Quersumme = 0;
		while(Zahl > 0){
		  Quersumme += (Zahl % 10);
		  Zahl /= 10;
		}
		System.out.println("Ergebnis ist: " + Quersumme);
	
	}
}
