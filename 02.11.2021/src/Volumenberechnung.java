import java.util.Scanner;
public class Volumenberechnung {

	public static void main(String[] args) {
		
		Scanner scannerWürfel = new Scanner(System.in);
		Scanner scannerQuader = new Scanner(System.in);
		Scanner scannerPyramide = new Scanner(System.in);
		Scanner scannerKugel = new Scanner(System.in);
		//Aufgabe a)
		System.out.println("Was ist der Wert?");
		double w_a = scannerWürfel.nextDouble();
		double ergebnis = w_a * w_a * w_a;
		System.out.println("Das Ergebnis lautet: " + ergebnis);
		
		//Aufgabe b)
		System.out.println("Was ist der erste Wert?");
		double q_a = scannerQuader.nextDouble();
		System.out.println("Was ist der zweite Wert?");
		double q_b = scannerQuader.nextDouble();
		System.out.println("Was ist der dritte Wert?");
		double q_c = scannerQuader.nextDouble();
		double ergebnis_q = q_a * q_b * q_c;
		System.out.println("Das Ergebnis lautet: " + ergebnis_q);
	}
	static double wuerfelberechnung(double w_a) {
		return  (w_a * w_a * w_a);
	}
	
	static double quaderberechnung(double q_a, double q_b, double q_c) {
		return (q_a * q_b * q_c);
	}
}
