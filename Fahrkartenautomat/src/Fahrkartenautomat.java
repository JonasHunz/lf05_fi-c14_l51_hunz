import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag = 0.00; 
       double eingezahlterGesamtbetrag = 0.00;
       double eingeworfeneM�nze = 0.00;
       double r�ckgabebetrag;
       double fahrkarte;
   	   double einzelfahrschein = 2.90;
   	   double tagesfahrschein = 8.60;
   	   double klTagesfahrschein = 23.50;
       double anzahlDerTickets = 0.00;
       
       do {
       fahrkarte = fahrkartenbestellungErfassen();
       

       
       //farkartenanzahl
       anzahlDerTickets = fahrkartenAnzahlAusw�hlen();
       
       switch((int)fahrkarte) {
   	case 1:
   		zuZahlenderBetrag = (einzelfahrschein * anzahlDerTickets);
   		System.out.println("\n" + "Zu zahlender Betrag:" + (einzelfahrschein * anzahlDerTickets ));
   		break;
   	case 2: 
   		zuZahlenderBetrag = (tagesfahrschein * anzahlDerTickets);
   		System.out.println("\n" + "Zu zahlender Betrag:" + (tagesfahrschein * anzahlDerTickets ));
   		break;
   	case 3: 
   		zuZahlenderBetrag = (klTagesfahrschein * anzahlDerTickets);
   		System.out.println("\n" + "Zu zahlender Betrag:" + (klTagesfahrschein * anzahlDerTickets ));
   		break;
       }
       
       // Geldeinwurf
       // -----------
       r�ckgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag, anzahlDerTickets);
      	  

       // Fahrscheinausgabe
       // -----------------
       fahrkartenAusgeben();

       // R�ckgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgeldAusgeben(r�ckgabebetrag);

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.\n\n\n");

       }while(true);
    }
    
    public static double fahrkartenbestellungErfassen()
    {
    	
    	Scanner tastatur = new Scanner(System.in);
    	
    	double einzelfahrschein = 2.90;
    	double tagesfahrschein = 8.60;
    	double klTagesfahrschein = 23.50;
    	double anzahlDerTickets = 0.00;
    	int auswahl = 0;
    	
    	System.out.println("W�hlen Sie ihre Wunschfahrkarte f�r Berlin AB aus:");
    	System.out.println("Einzelfahrschein Regeltarif AB " + einzelfahrschein + "0 Euro " + "(1)");
    	System.out.println("Tageskarte Regeltarif AB " + tagesfahrschein + "0 Euro " + "(2)");
    	System.out.println("Kleingruppen-Tageskarte Regeltarif AB " + klTagesfahrschein + "0 Euro " + "(3)");
    	
    	System.out.println("Geben Sie eine Zahl von 1 bis 3 ein, um das Ticket auszuw�hlen!");
    	System.out.print("Auswahl: ");
    	
    	
    	do {
   		auswahl = tastatur.nextInt();
    	switch(auswahl) {
    	case 1:
    	case 2:
    	case 3:
   			System.out.println("\n" + "Du hast die Fahrkarte " + auswahl + " gew�hlt");
    		
    		
    		default:
    			System.out.println("Ung�ltige Eingabe, bitte erneut versuchen!");
    			break;
    	}
   		
    	
    	}while(!(auswahl == 1 || auswahl == 2 || auswahl == 3));
    		
    	return (double)auswahl;
        
    }
    
    static double fahrkartenAnzahlAusw�hlen() {
    	Scanner tastatur = new Scanner(System.in);
		System.out.print("Anzahl der Tickets: ");
	    double anzahlDerTickets = tastatur.nextDouble();
    	return anzahlDerTickets;
    }
    
    
    static double fahrkartenBezahlen(double zuZahlenderBetrag, double anzahlDerTickets)
    {
    	Scanner tastatur = new Scanner(System.in);
    	double eingeworfeneM�nze;
        double eingezahlterGesamtbetrag;
        
        
    	eingezahlterGesamtbetrag = 0.00;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.println("Noch zu zahlen: " +  (zuZahlenderBetrag - eingezahlterGesamtbetrag) + "0");
     	   System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
     	   eingeworfeneM�nze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneM�nze;
        }
        
        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }
    
    static void fahrkartenAusgeben()
    {
    	
     	System.out.println("\n" + " Fahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
           try {
 			Thread.sleep(250);
 		   } 
           catch (InterruptedException e) 
           {
 			e.printStackTrace();
 		   }
        }
        System.out.println("\n\n");
    }
    
    static void rueckgeldAusgeben(double r�ckgabebetrag)
    {
    	
        if(r�ckgabebetrag > 0.00)
        {
     	   System.out.println("Der R�ckgabebetrag in H�he von " + r�ckgabebetrag + "0" + " EURO");
     	   System.out.println("wird in folgenden M�nzen ausgezahlt: ");

            while(r�ckgabebetrag >= 2.00) // 2 EURO-M�nzen
            {
         	  System.out.println("2 EURO");
 	          r�ckgabebetrag -= 2;
            }
            while(r�ckgabebetrag >= 1.00) // 1 EURO-M�nzen
            {
         	  System.out.println("1 EURO");
 	          r�ckgabebetrag -= 1;
            }
            while(r�ckgabebetrag >= 0.50) // 50 CENT-M�nzen
            {
         	  System.out.println("50 CENT");
 	          r�ckgabebetrag -= 0.50;
            }
            while(r�ckgabebetrag >= 0.20) // 20 CENT-M�nzen
            {
         	  System.out.println("20 CENT");
  	          r�ckgabebetrag -= 0.20;
            }
            while(r�ckgabebetrag >= 0.10) // 10 CENT-M�nzen
            {
         	  System.out.println("10 CENT");
 	          r�ckgabebetrag -= 0.10;
            }
            while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
            {
         	  System.out.println("5 CENT");
  	          r�ckgabebetrag -= 0.05;
            }
        }
    }
}