import java.util.Scanner;
public class RömischeZahlen {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie ein römisches Zahlenzeichen ein: ");
		System.out.println("I, V, X, C, D, M\nBitte verwenden Sie nur Großbuchstaben!" );
		String rom = myScanner.next();
		
		
		
		if(rom.equals("I")) {
			System.out.println("1");
		}
		
		if(rom.equals("V")) {
			System.out.println("5");
		}
		
		if(rom.equals("X")) {
			System.out.println("10");
		}
		
		if(rom.equals("L")) {
			System.out.println("50");
		}
		
		if(rom.equals("C")) {
			System.out.println("100");
		}
		
		if(rom.equals("D")) {
			System.out.println("500");
		}
		if(rom.equals("M")){
			System.out.println("1000");
		}
		myScanner.close();

	}

}
