import java.util.Scanner;
public class Noten {

	public static void main(String[] args) {
	int i = 0;
	Scanner myScanner = new Scanner(System.in);
	
	System.out.println("Geben Sie eine Note ein: ");
	int note = myScanner.nextInt();
	
	if(note == 1){
		System.out.println("Sehr gut");
	}
	if (note == 2){
		System.out.println("Gut");
	}
	if (note == 3) {
		System.out.println("Befriedigend");
	}
	if (note == 4) {
		System.out.println("Ausreichend");
	}
	if (note == 5) {
		System.out.println("Mangelhaft");
	}
	if (note == 6) {
		System.out.println("Ungenügend");
	}
	
	myScanner.close();
	}

}
