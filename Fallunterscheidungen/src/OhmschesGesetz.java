import java.util.Scanner;
public class OhmschesGesetz {

	public static void main(String[] args) {
		int ergebnis;
		boolean run = true;
		Scanner myScanner = new Scanner(System.in);
		
		
		while (run) {
			int R;
			int U;
			int I;
			System.out.println("Welche Gr��e soll berechnet werden? \nW�hlen Sie zwischen R, U und I");
			char ohm = myScanner.next().charAt(0);
			
			if(ohm == 'R') {
				System.out.println("Geben Sie die Werte f�r I und U ein");
				System.out.print("I: " ); 
				I = myScanner.nextInt();
				System.out.print("U: ");
				U = myScanner.nextInt();
				
				ergebnis = U / I;
				System.out.println("R:" + ergebnis);
			}
			else if(ohm == 'U') {
				System.out.println("Geben Sie die Werte f�r R und I ein");
				System.out.print("R: " ); 
				R = myScanner.nextInt();
				System.out.print("I: ");
				I = myScanner.nextInt();
				
				ergebnis = R * I;
				System.out.println("U:" + ergebnis);
				
			}
			else if(ohm == 'I') {
				System.out.println("Geben Sie die Werte f�r R und U ein");
				System.out.print("R: " ); 
				R = myScanner.nextInt();
				System.out.print("U: ");
				U = myScanner.nextInt();
				
				ergebnis = U / R;
				System.out.println("I:" + ergebnis);
			}
			
			if((ohm != 'I' && ohm != 'U' && ohm != 'R')) {
				System.out.println("Falsche Eingabe!");
			}
			String answer;
			System.out.println("M�chten Sie eine neue Rechnung durchf�hren?");
		 	answer = myScanner.next(); 
		 	if(answer.equals("Nein")) System.out.print("Auf wiedersehen!");  

		 	run = answer.equals("Ja");
			
		}
				
		myScanner.close();
	}

}
