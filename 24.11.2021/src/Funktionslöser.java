import java.util.Scanner;

public class Funktionsl�ser {

	public static void main(String[] args) {
		double e = 2.718;
		
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Geben Sie einen Wert f�r x ein: ");
		double x = myScanner.nextDouble();
		
		if(x <= 0) {
			
			System.out.println("Exponentieller Wert");
			System.out.println("f(x) = " + e*e);
		}
		if(0 < x && x <= 3) {
			System.out.println("Quadratischer Wert");
			System.out.println("f(x) = " + x*x +1);
		}

		
		if(x > 3) {
			System.out.println("Linearer Wert");
			System.out.println("f(x) = " + 2*x+4);
		}
		myScanner.close();
}
}
	